//
// Created by Emanuel on 08.03.2017.
//

#ifndef T3_FUSSBALLFELD_SPIELFELD_H
#define T3_FUSSBALLFELD_SPIELFELD_H

#include <iostream>
#include "Matrix.h"

class Matchfield {

    // - - - member variables - - - - - - - - - - - - - - - - - - - - - - - - - - -
    public:


    private:
        size_t length_m;
        size_t width_m;
        Matrix feld_m;
        static enum Direction { LEFT, RIGHT, UP, DOWN };

    // - - - methods  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    public:
        Matchfield(size_t length, size_t width);

    private:
        void initGoal(size_t length, Direction border);

};


#endif //T3_FUSSBALLFELD_SPIELFELD_H
