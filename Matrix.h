//
// Created by Emanuel on 08.03.2017.
//

#ifndef T3_FUSSBALLFELD_MATRIX_H
#define T3_FUSSBALLFELD_MATRIX_H


#include <iostream>
#include <vector>

class Matrix
{
    // - - - type definitions - - - - - - - - - - - - - - - - - - - - - - - - - - -

public:
    typedef std::vector<std::vector<char>> data_t;

    // - - - member variables - - - - - - - - - - - - - - - - - - - - - - - - - - -
public:


private:
    data_t data_m;
    size_t rows_m;
    size_t cols_m;

    // - - - methods  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
public:
    Matrix(size_t rows, size_t cols, char val);

    void outputMatrix();
    void setData(const data_t& matr);

    size_t rows();
    size_t cols();


    char& operator() (const size_t row_a, const size_t col_a) ;
    const char& operator() (const size_t row_a, const size_t col_a) const;

private:


};


#endif //T3_FUSSBALLFELD_MATRIX_H
